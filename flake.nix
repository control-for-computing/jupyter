{
  description = "A very basic flake";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/22.11";

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in
    rec {
      devShells.${system} = {
        default = pkgs.mkShell {
          buildInputs = [
            pkgs.gcc
            pkgs.gnumake
          ];
        };

        build_wheel = pkgs.mkShell {
          buildInputs = [
            pkgs.python3
            pkgs.python3Packages.pip
            pkgs.python3Packages.wheel
            pkgs.python3Packages.build
            pkgs.python3Packages.twine
            pkgs.python3Packages.setuptools
          ];
        };

        jpy = pkgs.mkShell {
          buildInputs = [
            pkgs.python3Packages.jupyterlab
            pkgs.python3Packages.matplotlib
            pkgs.python3Packages.ipywidgets
            pkgs.python3Packages.jupyterlab-widgets
          ];
        };
      };
    };
}
