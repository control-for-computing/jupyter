{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "42158925-19a3-42b7-9a2f-420979520fd3",
   "metadata": {},
   "source": [
    "# Controller FeedForward"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b5a41c0-1616-4475-9dd9-bb46c36a0b7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install ipywidgets==8.0.6\n",
    "from tuto_control_lib.systems import IntroSystem, IntroPerturbatedSystem\n",
    "from tuto_control_lib.plot import *\n",
    "from tuto_control_lib.utils import log\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from math import exp, pi, cos\n",
    "from statistics import mean\n",
    "from ipywidgets import interact\n",
    "\n",
    "print(\"Successful loading!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44b2cfcf",
   "metadata": {},
   "source": [
    "# When disturbance occurs\n",
    "\n",
    "A **disturbance**, often denotes $d$, is an uncontrolled signal that will influence our system even when we perform no action. \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "200f1dfc",
   "metadata": {},
   "outputs": [],
   "source": [
    "max_iter = 50\n",
    "system = IntroPerturbatedSystem()\n",
    "\n",
    "y_values = []\n",
    "u_values = []\n",
    "u = 0\n",
    "d = 0\n",
    "for i in range(max_iter):\n",
    "    y = system.sense()\n",
    "    y_values.append(y)\n",
    "    \n",
    "    u = 0.5\n",
    "    u_values.append(u)\n",
    "    system.apply(u)\n",
    "    \n",
    "plot_u_y_d(u_values, y_values)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d5b7bda3",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "  How can you characterize the effect of the disturbance ?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ebc5c6f",
   "metadata": {},
   "source": [
    "# Disturbance rejection \n",
    "\n",
    "In most cases, we would like that the impact of the disturbance on our sensor output to be small. \n",
    "\n",
    "We can tune the control signal to **compensate for the disturbance effects**, that is called disturbance rejection.\n",
    "\n",
    "Let's see first what happens if we **reuse our PI** for disturbance rejection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9235ea45-ac24-49eb-a685-c75508d78f14",
   "metadata": {},
   "outputs": [],
   "source": [
    "@interact(kp=(0, 5, 0.1), ki=(0, 5, 0.1))\n",
    "def show_pi(kp=2.5, ki=1.5):\n",
    "    max_iter = 50\n",
    "    reference_value = 1\n",
    "\n",
    "    y_values, u_values, u, system, integral = [], [], 0, IntroPerturbatedSystem(), 0\n",
    "\n",
    "    for _ in range(max_iter):\n",
    "        y = system.sense()\n",
    "        y_values.append(y)\n",
    "\n",
    "        error = reference_value - y\n",
    "        integral += error\n",
    "        u = kp * error + ki * integral\n",
    "\n",
    "        system.apply(u)\n",
    "        u_values.append(u)\n",
    "\n",
    "    plot_u_y_d(u_values, y_values, reference_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ddf22d4",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "  Can you find a good tuning, that keeps the output to the reference even when distrubance occurs ?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c206f2e",
   "metadata": {},
   "source": [
    "# Anticipating with FeedForward\n",
    "\n",
    "The PI is limitied for disturbance rejection as it is purely **reactive**. Indeed, the action is modified when the error changes, which may be too late to properly handle the disturbance.\n",
    "\n",
    "Another type of controller can thus be used: the feedforward controller. \n",
    "\n",
    "The general idea is to compute an action $u$ that will directly compensate the distrubance $d$ (rather than based on its impact on $y$).\n",
    "This requires that the disturbance signal can be measured, and that a model of its impact on the output is available. It also relies on the hypothesis that the control acts faster than the disturbance.\n",
    "\n",
    "The feedforward action, **compensating the disturbance before its effect can be seen in the output**, can be formulated as:\n",
    "\n",
    "$$\n",
    "u(k) = -K_{FF}  d(k)\n",
    "$$\n",
    "\n",
    "with $K_{FF}$ the feedforward gain.\n",
    "\n",
    "The above formula can be extended to also compensate the **dynamic** variations. In the case of first order systems with parameters $a$, $b$ for the system and $a_b$, $b_d$ for the disturbance effect, one has:\n",
    "\n",
    "$$\n",
    "u(k) = - \\dfrac{b_d}{b} [ d(k) - a *d(k-1) ] + a_d u(k-1)\n",
    "$$\n",
    "\n",
    "Let's see the effect of the feedforward on our system. Hints on how this formula ensures disturbance rejection is given bellow.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3306f762",
   "metadata": {},
   "outputs": [],
   "source": [
    "max_iter = 50\n",
    "reference_value = 1\n",
    "a = 0.8\n",
    "b = 0.5\n",
    "a_d = 0.4\n",
    "b_d = 0.4\n",
    "\n",
    "y_values, u_values, d_values, u, system, integral, d_old, u_old = [], [], [], 0, IntroPerturbatedSystem(), 0, 0, 0\n",
    "\n",
    "for _ in range(max_iter):\n",
    "    y = system.sense()\n",
    "    y_values.append(y)\n",
    "\n",
    "    d = system.sense_disturbance()\n",
    "    d_values.append(d)\n",
    "\n",
    "    error = reference_value - y\n",
    "    integral += error\n",
    "    u = - b_d/b * (d - a * d_old ) + a_d * u_old \n",
    "\n",
    "    u_old = u\n",
    "    d_old = d\n",
    "\n",
    "    system.apply(u)\n",
    "    u_values.append(u)\n",
    "\n",
    "plot_u_y_d(u_values, y_values, reference_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37f5b2f6",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "  Is the disturbance rejection better than with a PI ? What about reference tracking ? \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a64100f5",
   "metadata": {},
   "source": [
    "# Combining Controllers\n",
    "\n",
    "Feedforward control is often combined with feedback controllers, to ensure the tracking of the reference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7acd29e",
   "metadata": {},
   "outputs": [],
   "source": [
    "@interact(kp=(0, 5, 0.1), ki=(0, 5, 0.1))\n",
    "def show_pi(kp=0, ki=0):\n",
    "    max_iter = 50\n",
    "    reference_value = 1\n",
    "    a = 0.8\n",
    "    b = 0.5\n",
    "    a_d = 0.4\n",
    "    b_d = 0.4\n",
    "\n",
    "    y_values, u_values, d_values, u, system, integral, d_old, u_old = [], [], [], 0, IntroPerturbatedSystem(), 0, 0, 0\n",
    "\n",
    "    for _ in range(max_iter):\n",
    "        y = system.sense()\n",
    "        y_values.append(y)\n",
    "\n",
    "        d = system.sense_disturbance()\n",
    "        d_values.append(d)\n",
    "\n",
    "        error = reference_value - y\n",
    "        integral += error\n",
    "        u = - b_d/b * (d - a * d_old ) + a_d * u_old + kp * error + ki * integral\n",
    "\n",
    "        u_old = u\n",
    "        d_old = d\n",
    "\n",
    "        system.apply(u)\n",
    "        u_values.append(u)\n",
    "\n",
    "    plot_u_y_d(u_values, y_values, reference_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9203486",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "  Try to tune the PI to reach 0 steady state error. What can you say about the values of $K_p$ and $K_i$ ?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce8770eb",
   "metadata": {},
   "source": [
    "# Understanding the Feedforward formula\n",
    "\n",
    "Given the model of the system $y(k+1) = a y(k) + b u(k)$ and of the disturbance $y_d(k+1) = a_d y_d(k) + b_d d(k)$, show that with the action $u(k) = - \\dfrac{b_d}{b} [ d(k) - a *d(k-1) ] + a_d u(k-1)$, we have compensation (i.e. $y = -y_d$) *in steady state* (that is $y(k) = y(k+1) = y(k-1)$, same for $y_d$)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09f52312",
   "metadata": {},
   "source": [
    "[Back to menu](./00_Main.ipynb) or [Next chapter](./09_Methodology.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
