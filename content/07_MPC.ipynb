{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5ecd8455-ffcf-4230-adaf-6514c725b40b",
   "metadata": {},
   "source": [
    "# Model-Predictive Control"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbfec597",
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install ipywidgets==8.0.6\n",
    "from tuto_control_lib.systems import IntroSystem\n",
    "from tuto_control_lib.plot import *\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from ipywidgets import interact\n",
    "from scipy.optimize import fmin\n",
    "\n",
    "print(\"Successful loading!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b9d0e81",
   "metadata": {},
   "source": [
    "# An Optimal Controller\n",
    "\n",
    "Optimal control is an alternative to PID controllers. Rather than computing the action as a function of the error, it is the solution of an optimization problem. Optimal control consists in finding the **control actions that optimize a cost function**, often formulated based on a performance measure and relying on the system's model.\n",
    "\n",
    "For linear systems, there is an analytical solution. For non-linear systems (e.g. if we have a constraint on the control action) the optimal solution can be approached numerically. **Model-Predictive Control (MPC)** is a form of optimal control that allows a **trade-off between optimality and computation cost**, by considering optimization on a finite horizon.\n",
    "\n",
    "# Model-Predictive Control\n",
    "\n",
    "## Principle\n",
    "\n",
    "A Model-Predictive Controller consists in the following steps:\n",
    "1. **Predict** the future evolution of the output, given the model and hypothetical control actions, over a finite horizon,\n",
    "2. **Optimize** a cost function by soundly selecting a series of actions,\n",
    "3. **Apply the first action**, and \n",
    "4. **Repeat** each time step.\n",
    "\n",
    "Applying only the first action and optimizing at each step has the benefits of a **feedback approach**: it can correct uncertainties in the predictions or the model, and compensate for some disturbances.\n",
    "\n",
    "## Cost function\n",
    "\n",
    "The usual formulation of the MPC cost function is designed to ensure reference tracking (the output signal should converge to the reference value $y - y_{ref}$) with reasonable efforts (reasonable actions $|u|$):\n",
    "\n",
    "$$\n",
    "J = \\omega  \\sum_{i=1}^{H} (y(k+i) - y_{ref})^2  + \\sum_{i=1}^{H} u(k+i)^2 \n",
    "$$\n",
    "\n",
    "A weighting factor $\\omega$ allows specifying the relative importance of both objectives. \n",
    "$H$ is the lenght of the prediction horizon.\n",
    "\n",
    "Let us define the above cost function, replacing all future values of the output by their prediction using the model (e.g. $a$ and $b$):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "55c4897b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def cost_function(u, parameters):  \n",
    "    y_old, reference_value, a, b, weight = parameters\n",
    "    reference_tracking = 0\n",
    "    action_cost = 0\n",
    "    H = np.size(u)\n",
    "    for i in range(H): \n",
    "        sum_actions = 0\n",
    "        for j in range(i+1):\n",
    "            sum_actions += a**(i-j)*u[j]\n",
    "        reference_tracking += (a**(i+1)*y_old + b*sum_actions - reference_value)**2\n",
    "        action_cost += u[i]**2\n",
    "    cost_value = weight*reference_tracking + action_cost\n",
    "    return cost_value"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f074aecb",
   "metadata": {},
   "source": [
    "## Implementation \n",
    "\n",
    "At each time step, the MPC consists in:\n",
    "1. Computing the action trajectory that minimizes the cost over the horizon $H$: \n",
    "$$u(1:H) = argmin (J)$$\n",
    "2. apply $u(1)$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24a4b359",
   "metadata": {},
   "outputs": [],
   "source": [
    "@interact(H=(1, 5, 1), weight=(1,20,1))\n",
    "def draw_p(H=2, weight=10):\n",
    "    system, u_values, y_values, a, b, max_iter = IntroSystem(), [], [], 0.8, 0.5, 100\n",
    "\n",
    "    reference_value = 1\n",
    "    for i in range(max_iter):\n",
    "        y = system.sense()\n",
    "        y_values.append(y)\n",
    "\n",
    "        u = fmin(cost_function,np.ones((H,1)),args=(np.array([y, reference_value, a, b, weight]),), disp=0)\n",
    "\n",
    "        system.apply(u[0])\n",
    "        u_values.append(u[0])\n",
    "    plot_u_y(u_values, y_values, reference_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3053e74f",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">    \n",
    " Try different values of the horizon and the weight factor. How does the controller perform compared to a PID ?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c4ab817",
   "metadata": {},
   "source": [
    "### Bibliography\n",
    "\n",
    "Kouvaritakis, B., & Cannon, M. (2016). Model predictive control. Switzerland: Springer International Publishing, 38, 13-56."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea0f209d",
   "metadata": {},
   "source": [
    "[Back to menu](./00_Main.ipynb) or [Next chapter](./08_FeedForward.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
