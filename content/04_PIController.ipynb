{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "121c4163-0c06-4c32-a7ba-11cc51acfdff",
   "metadata": {},
   "source": [
    "# Proportional-Integral Controller"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e8c50d8-d7b2-4d85-aab7-f418755d42da",
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install ipywidgets==8.0.6\n",
    "from tuto_control_lib.systems import IntroSystem\n",
    "from tuto_control_lib.plot import *\n",
    "from tuto_control_lib.utils import log\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from math import exp, pi, cos\n",
    "from statistics import mean\n",
    "from ipywidgets import interact\n",
    "\n",
    "print(\"Successful loading!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c222167-167e-456d-bddf-4f7bde57d923",
   "metadata": {},
   "source": [
    "# Integration action for better precision\n",
    "\n",
    "As we have seen before, a Proportional controller is inherently imprecise.\n",
    "\n",
    "One way to improve the precision of the closed loop system is to add an integral term to the controller. This results in a Proportional Integral Controller, where the control action is updated based on the error (as in the Proportional Controller) *and* on the *integral* of the error.\n",
    "\n",
    "The integral term allows canceling the steady state error.\n",
    "\n",
    "The form of the controller (in discrete time) is the following:\n",
    "\n",
    "$$\n",
    "u(k) = K_p e(k) + K_i \\sum_{i=0}^k e(i)\n",
    "$$\n",
    "\n",
    "Where we see that a term with an integral gain $K_i$ is added to the formulation of the previously defined P Controller. Here again, we will have to finely tune the $K_p$ and $K_i$ parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "df65cb57-486d-414d-aae7-e747167d3a91",
   "metadata": {},
   "outputs": [],
   "source": [
    "@interact(kp=(0, 5, 0.1), ki=(0, 5, 0.1))\n",
    "def show_pi(kp=2.5, ki=1.5):\n",
    "    max_iter = 50\n",
    "    reference_value = 1\n",
    "\n",
    "    y_values, u_values, u, system, integral = [], [], 0, IntroSystem(), 0\n",
    "\n",
    "    for _ in range(max_iter):\n",
    "        y = system.sense()\n",
    "        y_values.append(y)\n",
    "\n",
    "        error = reference_value - y\n",
    "        integral += error\n",
    "        u = kp * error + ki * integral\n",
    "\n",
    "        system.apply(u)\n",
    "        u_values.append(u)\n",
    "\n",
    "    plot_u_y(u_values, y_values, reference_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb99ce6a-f051-4074-b049-5f5ed3273628",
   "metadata": {},
   "source": [
    "We can see that the system converges to the reference value!\n",
    "The precision is good: the error in steady state is null."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb414899-941e-4652-93c1-df6ab939c6f8",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "  Try to change the values of $K_p$ and $K_i$.\n",
    "    \n",
    "  How do they impact the stability, precision, settling time and overshoot?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc185bc1-7b31-41df-8dd3-b260aca6097b",
   "metadata": {},
   "source": [
    "# Design of the PI Controller"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8df0233-6f1c-487d-8861-9de5d432aa9a",
   "metadata": {},
   "source": [
    "As for the P Controller, we have to choose the suitable values for the controller parameters $K_p$ and $K_i$. We fill find rules to compute them based on the behavior we want for our closed-loop. We recall that the behavior is characterized by the 4 criteria of stability, precision, settling time and maximum overshoot.\n",
    "\n",
    "In the case of a PI Controller, the **precision** is mathematically ensured by the integral term if $K_i \\neq 0$.\n",
    "\n",
    "There are several methods to find gains for a PI Controller.\n",
    "In the following, we use the *pole placement method*.\n",
    "This time we will skip the mathematical explanations, and we only give the final equations leading to the gains.\n",
    "\n",
    "Given the desired values for $k_s$, the **settling time**, $M_p$ the **maximum overshoot**, and requirement of **stability**, we get:\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "K_p = \\frac{a - r^2}{b}\\\\\n",
    "K_i = \\frac{1 - 2 r \\cos \\theta + r^2}{b}\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "with:\n",
    "\n",
    "- $r = \\exp\\left(-\\frac{4}{k_s}\\right)$\n",
    "- $\\theta = \\pi \\frac{\\log r}{\\log M_p}$\n",
    "\n",
    "In our case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d6de7c9-d0a9-483a-9d70-eb03d49c1583",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The coefficients of our system\n",
    "a = 0.8\n",
    "b = 0.5\n",
    "\n",
    "# Our desired properties\n",
    "ks = 10\n",
    "mp = 0.1\n",
    "\n",
    "@interact(ks=(0, 10, 1), mp=(0, 0.9, 0.05))\n",
    "def show_pi_gains(ks=ks, mp=mp):\n",
    "    r = exp(-4/ks)\n",
    "    theta = pi * log(r) / log(mp)\n",
    "\n",
    "    kp = (a - r * r) / b\n",
    "    ki = (1 - 2 * r * cos(theta) + r * r) / b\n",
    "\n",
    "    print(f\"Kp = {kp}\\nKi = {ki}\")\n",
    "\n",
    "    max_iter = 50\n",
    "    reference_value = 1\n",
    "    y_values, u_values, u, system, integral_error = [], [], 0, IntroSystem(), 0\n",
    "\n",
    "    for i in range(max_iter):\n",
    "        y = system.sense()\n",
    "        y_values.append(y)\n",
    "\n",
    "        error = reference_value - y\n",
    "        integral_error += error\n",
    "        u = kp * error + ki * integral_error\n",
    "\n",
    "        system.apply(u)\n",
    "        u_values.append(u)\n",
    "\n",
    "    plot_u_y(u_values, y_values, reference_value)\n",
    "    e_ss = reference_value - y_values[-1]\n",
    "    max_overshoot = (max(y_values) - y_values[-1]) / y_values[-1]\n",
    "    settling_time = len([x for x in y_values if abs(x - y_values[-1]) > 0.05])\n",
    "\n",
    "    print(f\"Precision: {e_ss}\")\n",
    "    print(f\"Settling Time: {settling_time} -> desired: < {ks}\")\n",
    "    print(f\"Max. Overshoot: {max_overshoot} -> desired: < {mp}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41159696-e75e-4ae5-8b69-44799bf482d9",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "  Try to change the requirements on the closed-loop properties.\n",
    "    \n",
    "  Does the controller manage to fulfill your requirements ? \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8e31f05-7d99-4793-801e-51c292c7c8ec",
   "metadata": {},
   "source": [
    "[Back to menu](./00_Main.ipynb) or [Next chapter](./05_PIDController.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
