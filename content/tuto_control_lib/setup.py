from setuptools import setup

setup(
    name="tuto_control_lib",

    version="1.3.1",

    author="Quentin Guilloteau",
    author_email="Quentin.Guilloteau@inria.fr",
    
    url = "",


    description="Helper lib for tuto control for computing",

    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    install_requires=[
    ],

    packages = ["cigri", "."],
)

