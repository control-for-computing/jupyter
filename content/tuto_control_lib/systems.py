import random
import math

class System:
    def __init__(self, a, b, y0=0, noise=False):
        self.y = y0
        self.noise = noise
        self.f = lambda y, u: a * y + b * u
        self.iteration = 0
        
    def sense(self):
        if self.noise:
            y = self.y + random.random() / 10
            self.y = y if y > 0 else 0 
        return self.y
    
    def apply(self, u):
        self.y = self.f(self.y, u)
        self.iteration += 1
        return self
        
    def get_time(self):
        return self.iteration
        
class IntroSystem(System):
    def __init__(self):
        System.__init__(self, 0.8, 0.5, y0=0, noise=False)

class NoisySystem(System):
    def __init__(self):
        System.__init__(self, 0.8, 0.5, y0=0, noise=True)
        
class UnknownSystem(System):
    def __init__(self):
        System.__init__(self, 0.628, 0.314, y0=0, noise=True)

class SystemWithPerturbations:
    def __init__(self, main_system, perturbation_system):
        self.main_system = main_system
        self.perturbations_system = perturbation_system
        
    def sense(self):
        return self.main_system.sense() + self.perturbations_system.sense()
    
    def apply(self, u, d):
        self.main_system.apply(u)
        self.perturbations_system.apply(d)
        return self
        
    def get_time(self):
        return self.main_system.get_time()

class PerturbatedSystem(SystemWithPerturbations):
    def __init__(self, main_system=IntroSystem(), perturbations_system=IntroSystem(), d=0, step_start=0, step_end=math.inf):
        SystemWithPerturbations.__init__(self, main_system, perturbations_system)
        self.step_start = step_start
        self.step_end = step_end
        self.d = d
        
    def sense_disturbance(self):
        if self.main_system.get_time() >= self.step_start and self.main_system.get_time() <= self.step_end:
            return self.d
        else:
            return 0

    def apply(self, u):
        self.main_system.apply(u)
        if self.main_system.get_time() >= self.step_start and self.main_system.get_time() <= self.step_end:
            self.perturbations_system.apply(self.d)
        else:
            self.perturbations_system.apply(0)
        return self

class IntroPerturbatedSystem(PerturbatedSystem):
    def __init__(self):
        PerturbatedSystem.__init__(self, IntroSystem(), System(0.4, 0.4, y0=0, noise=False), d=2, step_start=20, step_end=35)
