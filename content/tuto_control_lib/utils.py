import math

def log(x):
    if x == 0:
        return -math.inf
    return math.log(x)
